package ua.com.novikdm.models;

import ua.com.novikdm.models.entity.Product;
import ua.com.novikdm.models.entity.Refrigerator;
import ua.com.novikdm.models.entity.enums.ClimateClass;
import ua.com.novikdm.models.entity.enums.ProductType;
import ua.com.novikdm.models.entity.enums.Sale;

import java.util.ArrayList;
import java.util.HashMap;

public class Database {
  private HashMap<ProductType, ArrayList<Product>> database;

  public Database() {
    database = new HashMap<>();
    startInitialization();
  }

  public HashMap<ProductType, ArrayList<Product>> getDatabase() {
    return database;
  }

  public boolean saveProduct(ProductType productType, Product product) {
    boolean savingResult;
    try {
      addProduct(productType, product);
      savingResult = true;
    }
    catch (Exception e) {
      savingResult = false;
    }
    return savingResult;
  }

  public ArrayList<Product> searchByBrand(String brand) {
    ArrayList<Product> resultProducts = new ArrayList<>();
    for (ProductType productType:
         ProductType.values()) {
      ArrayList<Product> productsFromDatabase =
             database.get(productType);
      if(productsFromDatabase != null) {
        for (Product product:
                productsFromDatabase) {
          if (product.getBrand().equals(brand)) {
            resultProducts.add(product);
          }
        }
      }
    }
    return resultProducts;
  }

  private void addProduct(ProductType productType, Product product) {
    ArrayList<Product> productsList = database.get(productType);
    if(productsList == null) {
      productsList = new ArrayList<>();
    }
    productsList.add(product);
    database.remove(productType);
    database.put(productType, productsList);
  }

  private void startInitialization() {
    Refrigerator ref = new Refrigerator(0, "ARDO", "R1", 123.1,
            "48216532486", Sale.DISCOUNT_0, 3, "Kitchen", 355,
            60, 180, 60, true, 155.5, 2, ClimateClass.CLASS_ST);
    Refrigerator ref1 = new Refrigerator(1, "ARDO", "R2", 156,
            "48216532487", Sale.DISCOUNT_0, 5, "Kitchen", 360,
            60, 180, 60, true, 158, 2, ClimateClass.CLASS_ST);
    Refrigerator ref2 = new Refrigerator(2, "Samsung", "Sam1", 264,
            "48211432325", Sale.DISCOUNT_0, 7, "Kitchen", 250,
            70, 200, 60, false, 160, 2, ClimateClass.CLASS_NT);
    Refrigerator ref3 = new Refrigerator(3, "Samaref", "RF700", 110000,
            "12354896524", Sale.DISCOUNT_25, 1, "Kitchen", 700,
            70, 198, 70, false, 230, 1, ClimateClass.CLASS_T);
    addProduct(ProductType.PRODUCT_REFRIGERATOR, ref);
    addProduct(ProductType.PRODUCT_REFRIGERATOR, ref2);
    addProduct(ProductType.PRODUCT_REFRIGERATOR, ref1);
    addProduct(ProductType.PRODUCT_REFRIGERATOR, ref3);
  }
}
