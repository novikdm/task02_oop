package ua.com.novikdm.controllers;

import ua.com.novikdm.models.Database;
import ua.com.novikdm.models.entity.Product;
import ua.com.novikdm.models.entity.enums.ProductType;

import java.util.ArrayList;
import java.util.HashMap;

public final class MainController {
  Database database = new Database();
  private static MainController mainController =
          new MainController();

  public static MainController getInstance() {
    return mainController;
  }

  public ArrayList<Product> findByBrand(String brand) {
    return database.searchByBrand(brand);
  }

  public HashMap<ProductType, ArrayList<Product>> getAllProducts() {
    return  database.getDatabase();
  }

  public void addProduct(ProductType productType, Product product) {
    database.saveProduct(productType, product);
  }
}
