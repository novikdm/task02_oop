package ua.com.novikdm.models.entity;

import ua.com.novikdm.models.entity.enums.Sale;

import java.util.Objects;

public abstract class Product {
  private int id;
  private String brand;
  private String model;
  private double price;
  private String barcode;
  private Sale sale;
  private int inStock;

  public Product(int id, String brand, String model, double price,
                 String barcode, Sale sale, int inStock) {
    this.id = id;
    this.brand = brand;
    this.model = model;
    this.price = price;
    this.barcode = barcode;
    this.sale = sale;
    this.inStock = inStock;
  }

  public int getId() {
    return id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getBarcode() {
    return barcode;
  }

  public void setBarcode(String barcode) {
    this.barcode = barcode;
  }

  public Sale getSale() {
    return sale;
  }

  public void setSale(Sale sale) {
    this.sale = sale;
  }

  public int getInStock() {
    return inStock;
  }

  public void setInStock(int inStock) {
    this.inStock = inStock;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Product product = (Product) o;
    return id == product.id &&
            Double.compare(product.price, price) == 0 &&
            inStock == product.inStock &&
            Objects.equals(brand, product.brand) &&
            Objects.equals(model, product.model) &&
            Objects.equals(barcode, product.barcode) &&
            sale == product.sale;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, brand, model, price, barcode, sale, inStock);
  }

  @Override
  public String toString() {
    return "Product{" +
            "id=" + id +
            ", brand='" + brand + '\'' +
            ", model='" + model + '\'' +
            ", price=" + price +
            ", barcode='" + barcode + '\'' +
            ", sale=" + sale +
            ", inStock=" + inStock +
            '}';
  }
}
