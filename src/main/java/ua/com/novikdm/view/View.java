package ua.com.novikdm.view;

import ua.com.novikdm.controllers.MainController;
import ua.com.novikdm.models.entity.Product;
import ua.com.novikdm.models.entity.Refrigerator;
import ua.com.novikdm.models.entity.enums.ClimateClass;
import ua.com.novikdm.models.entity.enums.ProductType;
import ua.com.novikdm.models.entity.enums.Sale;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class View {
  private static MainController controller = MainController.getInstance();

  public static void run() {
    runMainMenu();
  }

  private static void runMainMenu() {
    Scanner scanner = new Scanner(System.in);
    boolean whileTrigger = true;
    while (whileTrigger) {
      printMenu();
      switch (scanner.nextInt()) {
        case 1:
          System.out.println("1");
          break;
        case 2:
          printShowByBrandMenu();
          break;
        case 3:
          System.out.println("3");
          break;
        case 4:
          showAllProducts();
          break;
        case 5:
          runProductManagement();
          break;
        case 0:
          whileTrigger = false;
          break;
        default:
          System.out.println("Error! Try again!");
          printSeparator();
      }
    }
  }

  private static void runProductManagement() {
    Scanner scanner = new Scanner(System.in);
    boolean whileTrigger = true;
    while (whileTrigger) {
      printProductManagementMenu();
      switch (scanner.nextInt()) {
        case 1:
          System.out.println("5-1");
          break;
        case 2:
          addProduct();
          break;
        case 3:
          System.out.println("5-3");
          break;
        case 7:
          System.out.println("5-7");
          break;
        case 0:
          whileTrigger = false;
          break;
        default:
          System.out.println("Error! Try again!");
          printSeparator();
      }
    }
  }

  private static void printMenu() {
    printSeparator();
    System.out.println("MENU:");
    System.out.println("1 - Show products by category");
    System.out.println("2 - Show products by brand");
    System.out.println("3 - Sorts product by price");
    System.out.println("4 - Show all products");
    System.out.println("5 - Product management");
    System.out.println("0 - Exit");
    printSeparator();
  }

  private static void printProductManagementMenu() {
    printSeparator();
    System.out.println("Product management");
    System.out.println("1 - Show ending products");
    System.out.println("2 - Add product");
    System.out.println("0 - Exit");
    printSeparator();
  }

  private static void printProductType() {
    printSeparator();
    System.out.println("Product management");
    System.out.println("1 - Refrigerator");
    System.out.println("2 - Multicooker");
    System.out.println("0 - Exit");
    printSeparator();
  }
  private static void addProduct() {
    Scanner scanner = new Scanner(System.in);
    String brand = scanner.nextLine();
    printProductType();
    int numberOfProductType = scanner.nextInt();
    Product product = writeProduct();
    controller.addProduct(getType(numberOfProductType), product);
  }

  private static ProductType getType(int numberOfProductType) {
    ProductType resultType;
    switch (numberOfProductType) {
      case 1:
        resultType = ProductType.PRODUCT_REFRIGERATOR;
        break;
      case 2:
        resultType = ProductType.PRODUCT_MULTICOOKER;
        break;
      default:
        resultType = ProductType.PRODUCT_REFRIGERATOR;
    }
    return resultType;
  }

  private static Refrigerator writeProduct() {
    System.out.println("Write your product");
    Scanner scanner = new Scanner(System.in);
    System.out.println("ID:");
    int id = scanner.nextInt();
    System.out.println("Brand:");
    String brand = scanner.nextLine();
    System.out.println("Model:");
    String model = scanner.nextLine();
    System.out.println("Price:");
    double price = scanner.nextDouble();
    System.out.println("Barcode:");
    String barcode = scanner.nextLine();
    Sale discount = Sale.DISCOUNT_0;
    System.out.println("In Stock:");
    int inStock = scanner.nextInt();
    System.out.println("Type:");
    String type = scanner.nextLine();
    System.out.println("Power:");
    double power = scanner.nextDouble();
    System.out.println("Width:");
    double width = scanner.nextDouble();
    System.out.println("Height:");
    double height = scanner.nextDouble();
    System.out.println("Depth:");
    double depth = scanner.nextDouble();
    System.out.println("NoFrost");
    boolean isNoFrost = scanner.nextBoolean();
    System.out.println("Volume:");
    double volume = scanner.nextDouble();
    System.out.println("# chambers:");
    int chambers = scanner.nextInt();
    ClimateClass climateClass = ClimateClass.CLASS_N;
    return new Refrigerator(id, brand, model, price, barcode,
            discount, inStock, type, power, width, height, depth,
            isNoFrost, volume, chambers, climateClass);
  }

  private static void printShowByBrandMenu() {
    printSeparator();
    System.out.println("Show products by brand");
    System.out.println();
    System.out.println("Write brand:");
    ArrayList<Product> productsByBrand = getProductsByBrand();
    for (Product product:
         productsByBrand) {
      System.out.println(product);
    }
    printSeparator();
  }

  private static void printSeparator() {
    String separator =
            "-----------------------------------------------------------------";
    System.out.println(separator);
  }

  private static ArrayList<Product> getProductsByBrand() {
    Scanner scanner = new Scanner(System.in);
    String brand = scanner.nextLine();
    ArrayList<Product> resultProductsList =
            controller.findByBrand(brand);
    return resultProductsList;
  }

  private static void showAllProducts() {
    printSeparator();
    HashMap<ProductType, ArrayList<Product>> allProducts =
            controller.getAllProducts();
    for (ProductType productType:
            ProductType.values()) {
      ArrayList<Product> productsFromDatabase =
              allProducts.get(productType);
      if(productsFromDatabase != null) {
        for (Product product:
                productsFromDatabase) {
          System.out.println(product);
        }
      }
    }
  }
}
