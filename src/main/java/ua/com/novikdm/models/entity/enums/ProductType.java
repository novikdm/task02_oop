package ua.com.novikdm.models.entity.enums;

public enum ProductType {
  PRODUCT_REFRIGERATOR, PRODUCT_MULTICOOKER, PRODUCT_WASHINGMACHINE
}
