package ua.com.novikdm.models.entity.enums;

public enum Sale {
  DISCOUNT_0, DISCOUNT_5, DISCOUNT_10,
  DISCOUNT_15, DISCOUNT_20, DISCOUNT_25,
  DISCOUNT_30, DISCOUNT_50
}
