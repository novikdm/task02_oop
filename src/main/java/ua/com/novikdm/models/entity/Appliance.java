package ua.com.novikdm.models.entity;

import ua.com.novikdm.models.entity.enums.Sale;

import java.util.Objects;

public abstract class Appliance extends Product {
  private String type;
  private double power;
  private double width;
  private double height;
  private double depth;

  public Appliance(int id, String brand, String model, double price,
                   String barcode, Sale sale, int inStock,
                   String type, double power, double width,
                   double height, double depth) {
    super(id, brand, model, price, barcode, sale, inStock);
    this.type = type;
    this.power = power;
    this.width = width;
    this.height = height;
    this.depth = depth;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public double getPower() {
    return power;
  }

  public void setPower(double power) {
    this.power = power;
  }

  public double getWidth() {
    return width;
  }

  public void setWidth(double width) {
    this.width = width;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public double getDepth() {
    return depth;
  }

  public void setDepth(double depth) {
    this.depth = depth;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Appliance appliance = (Appliance) o;
    return Double.compare(appliance.power, power) == 0 &&
            Double.compare(appliance.width, width) == 0 &&
            Double.compare(appliance.height, height) == 0 &&
            Double.compare(appliance.depth, depth) == 0 &&
            Objects.equals(type, appliance.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), type, power, width, height, depth);
  }

  @Override
  public String toString() {
    return "Appliance{" +
            "type='" + type + '\'' +
            ", power=" + power +
            ", width=" + width +
            ", height=" + height +
            ", depth=" + depth +
            '}';
  }
}
