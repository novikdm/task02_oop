package ua.com.novikdm.models.entity;

import ua.com.novikdm.models.entity.enums.ClimateClass;
import ua.com.novikdm.models.entity.enums.Sale;

import java.util.Objects;

public class Refrigerator extends Appliance {
  private boolean isNoFrost;
  private double volume;
  private int numberOfChambers;
  private ClimateClass climateClass;

  public Refrigerator(int id, String brand, String model,
                      double price, String barcode, Sale sale,
                      int inStock, String type, double power,
                      double width, double height, double depth,
                      boolean isNoFrost, double volume,
                      int numberOfChambers,
                      ClimateClass climateClass) {
    super(id, brand, model, price, barcode, sale, inStock, type,
            power, width, height, depth);
    this.isNoFrost = isNoFrost;
    this.volume = volume;
    this.numberOfChambers = numberOfChambers;
    this.climateClass = climateClass;
  }

  public boolean isNoFrost() {
    return isNoFrost;
  }

  public void setNoFrost(boolean noFrost) {
    isNoFrost = noFrost;
  }

  public double getVolume() {
    return volume;
  }

  public void setVolume(double volume) {
    this.volume = volume;
  }

  public int getNumberOfChambers() {
    return numberOfChambers;
  }

  public void setNumberOfChambers(int numberOfChambers) {
    this.numberOfChambers = numberOfChambers;
  }

  public ClimateClass getClimateClass() {
    return climateClass;
  }

  public void setClimateClass(ClimateClass climateClass) {
    this.climateClass = climateClass;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Refrigerator that = (Refrigerator) o;
    return isNoFrost == that.isNoFrost &&
            Double.compare(that.volume, volume) == 0 &&
            numberOfChambers == that.numberOfChambers &&
            climateClass == that.climateClass;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), isNoFrost, volume, numberOfChambers, climateClass);
  }

  @Override
  public String toString() {
    return "Refrigerator{" +
            "isNoFrost=" + isNoFrost +
            ", volume=" + volume +
            ", numberOfChambers=" + numberOfChambers +
            ", climateClass=" + climateClass +
            '}';
  }
}
