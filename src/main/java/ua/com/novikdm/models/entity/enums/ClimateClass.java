package ua.com.novikdm.models.entity.enums;

public enum ClimateClass {
  CLASS_ST, CLASS_NT, CLASS_N, CLASS_T
}
